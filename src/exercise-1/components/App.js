import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router} from 'react-router-dom';
import Header from "./childrenComponent/Header";
import {Route} from "react-router";
import Home from "./childrenComponent/Home";
import MyProfile from "./childrenComponent/MyProfile";
import AboutUs from "./childrenComponent/AboutUs";

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <div>
            <Header />
            <Route exact path="/" component={Home} />
            <Route path="/my-profile" component={MyProfile} />
            <Route path="/about-us" component={AboutUs} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
