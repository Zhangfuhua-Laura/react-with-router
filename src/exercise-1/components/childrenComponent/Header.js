import React from "react";
import {NavLink} from "react-router-dom";

const Header = () => {
  return (
    <nav>
      <ul className='navList'>
        <li>
          <NavLink to="/" exact activeStyle={{textDecoration: "underline"}}>Home</NavLink>
        </li>
        <li>
          <NavLink to="/my-profile" activeStyle={{textDecoration: "underline"}}>My Profile</NavLink>
        </li>
        <li>
          <NavLink to="/about-us" activeStyle={{textDecoration: "underline"}}>About Us</NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Header;